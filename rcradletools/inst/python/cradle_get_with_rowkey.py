import sys
sys.path.append('/home/rxf131/pythonpackages/lib/python2.7.13/site-packages')
import pandas as pd
import crad_get as cg

def cradle_get_with_rowkey(table_name,sys_name, unique_file_num):
    #import pdb
    #pdb.set_trace()
    """
        This Function fetches data and writes the dataframe as a csv file,
        but with a rowkey as an extra column
        The data is fetched based oin table and sys_name, which is alphanumeric
        unique_file_num is to create a unique file

    Args:
        arg1 (string): Name of the table to retrieve from.
        arg2 (string): Alphanumeric which is the prefix of a rowkey
                       you are looking for.
        arg3 (string): It is the random file number such that crad_get
                       fun creates unique file for R to read. This
                       parameter is not not for end user.
    Returns:
        res (csv_file): retirved dataset.
    """
    rows = cg.data_download(table_name,sys_name)
    df_for_all_site = pd.DataFrame()
    df_for_each_site = pd.DataFrame()
    final_dict = {}
    for key,data in iter(sorted(rows.iteritems())):
        row_wise_df = pd.DataFrame()
        axis_flag = 1
        for i in data.keys():
            df1 = pd.DataFrame(data[i].strip("'").split(","), columns=[i])
            if axis_flag == 1:
                row_wise_df  = pd.concat([row_wise_df,df1])
                axis_flag = 0
            else:
                row_wise_df = pd.concat([row_wise_df,df1],axis=1)
            row_wise_df['row_key'] = key #Makes a new column for the Row Key
        col_list = list(row_wise_df.columns.values)

        #Since we added a new column,
        #the numbers must be adjusted to prevent an off by one error.
        row_wise_df.columns = [i.split(':', 0)[0] for i in col_list]

        df_for_each_site = pd.concat([df_for_each_site,row_wise_df])
        df_for_each_site['site'] = sys_name
    df_for_all_site = pd.concat([df_for_all_site ,df_for_each_site])
    #below line commented because, now we don't need to write in csv,
    #other wise just uncomment following line
    cg.write_csv_df(df_for_all_site,sys_name,unique_file_num)
    return df_for_all_site



if __name__ == '__main__':
    #call cradle_get_with_rowkey function with the three arguments
    cradle_get_with_rowkey(sys.argv[1],sys.argv[2],sys.argv[3])
