import sys
sys.path.append('/home/rxf131/pythonpackages/lib/python2.7.13/site-packages')
import happybase
import os
from os.path import basename
import glob

def cradle_put_bin(incoming_obj_file, table_name, family_name, col_name):
#def cradle_put_bin():

    """This function takes in four parameters.
       The purpose of the function is to ingest data from either dataframe
       or a csv file into HBase table.
       The filetypes handeled by this function are 
       ('*.jpg', '*.jpeg', '*.png', '*.spc','*.bmp', '*.Rda', '*pdf', '*.RData','*RDS','*srw').
       To add new filetype, edit this function to add new filetypes.
        
    Args:
        arg1 (string): Path of the binary object
        arg2 (string): Name of the table to ingest data into.
        arg3 (string): Family name in the table.
        arg4 (string): Column names of in which the files are ingested
    Returns:
        Nothing, Ingest data into HBase table
    Examples:
        I.  cradle_put_bin('/home/axk962/test.RData','result','bin', 'col_name')

    """

    connection = happybase.Connection('IP-ADDRESS',port=9090)##hbase connection ip
    table = connection.table(table_name)
    files = []
    #expected binary data type
    types = ('*.jpg', '*.jpeg', '*.png', '*.spc','*.bmp', '*.Rda', '*pdf', '*.RData','*RDS','*srw')
    ingested_files = []
    for extension in types:
        #files.extend(glob.glob(os.path.join(dir_path, extension)))
        file_name = incoming_obj_file
        #tag is the data type, which is appended before col_name
        #to keep track of data type in crad_get_bin function
        tag = file_name.split(".")[-1]
        #get the row_key name by spliting '/' to remove directory path
        #and also first 20 characters generated as random in R.
        name = file_name.split("/")[-1].split(".")[0][20:] 
        name = basename(name)
        if name not in ingested_files:
            with open(file_name, 'rb') as file:
                content = file.read()
                file.close()
                #Appends the file type extension into the column name and
                #puts data into HBase table.
                table.put(name, {b'' + family_name + ':'+tag+'_'+col_name: content})
            ingested_files.append(name)
    #sys.stdout.write("ingestion to HBase conpleted, closing connection... \n")
    connection.close()

if __name__ == '__main__':
    import pdb
    #pdb.set_trace()
    cradle_put_bin(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
