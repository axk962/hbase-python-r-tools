def cradle_get(table_name,sys_name):
    """This function takes in two parameters.
       The purpose of the function is to retrieve data from the data table,
       based on the alphanumeric. The first parameter is the table name of 
       the data table. The second parameter is the alphanumeric 'abc1234',
       such that it retrives data from all row keys of the pattern
       'abc1234-YYYYMM'.
        
    Args:
        arg1 (string): Name of the table to retrieve from. 
        arg2 (string/list): Name of the alphanumeric (which is prefix of 
                                                 the rowkey). 
    Returns:
        res (dataframe): retirved dataset.
    Examples:
        output_dataframe = cradle_get('ecradle','a4mwbbm')
    """
    import sys
    sys.path.append('/home/rxf131/pythonpackages/lib/python3.7.0/site-packages')
    from .crad_get import crad_get
    #import pdb
    #pdb.set_trace()

    if type(sys_name) is str:
        #convert 'str' to list
        sys_name = sys_name.split()
    dfr = crad_get(table_name,sys_name)
    return dfr
