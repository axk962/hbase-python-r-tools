import sys
sys.path.append('/home/rxf131/pythonpackages/lib/python3.7.0/site-packages')
import happybase
import glob
import pandas as pd
#import pdb
import re

def write_csv_df(df):
    filename = "python_to_r.csv"
    is_file_exist = glob.glob(filename)
    if not is_file_exist:
        df.to_csv(filename,sep='~',index=False)
    else:
        df.to_csv(filename,sep='~',index=False)
    #    overwrite = raw_input ("Warning: "+ filename + \
    #                " already  exists, would you want to overwrite (y/n) \n")
    #    if overwrite == 'y':
    #        df.to_csv(filename,sep=',',index=False)
    #    elif overwrite == "n":
    #        new_filename = raw_input("Type new filename \n")
    #        df.to_csv(new_filename,sep=',',index=False)
    #    else:
    #        print("Not a valid input, no file saved!\n")


def data_download(table_name,row_keys):
    #This function downloads data from HBase and returns it as a dictionary
    #pdb.set_trace()
    connection = happybase.Connection('IP-ADDRESS',port=9090)##hbase connection ip
    table = connection.table(str.encode(table_name))#connection to desired table
    l=[] #introducing empty list
    p=[]
    dictreturn={}
    # getting data by table.rows loop
    for row_key in row_keys:
        for key,data in table.scan(row_prefix = str.encode(row_key), batch_size = 1):
            dictreturn[key] = data
    from collections import defaultdict
    try:
        connection.close()
    except Exception as e:
        print (e)
        print ("\nUnable to close Hbase connection \n")
    return dictreturn


def crad_get(table_name,sys_name):
    #pdb.set_trace()
    #data_download functions returns dictionary
    #such that each key in dictionary is a row_key.
    rows = data_download(table_name,sys_name)
    df_for_all_site = pd.DataFrame()
    df_for_each_site = pd.DataFrame()
    final_dict = {}
    for key,data in iter(sorted(rows.items())):
        row_wise_df = pd.DataFrame()
        axis_flag = 1
        for i in data.keys():
            #if column name contains binary values, we skip when using crad_get
            # function, because this function returns dataframe & binary values
            # can not be stored in the timeseries dataframe.
            if re.match('ftir',i.decode("utf-8")) or \
                   re.match('spc',i.decode("utf-8")) or \
                   re.match('jpg',i.decode("utf-8")) or \
                   re.match('jpeg',i.decode("utf-8")) or \
                   re.match('bmp',i.decode("utf-8")) or \
                   re.match('image',i.decode("utf-8")):
                continue

            df1 = pd.DataFrame(data[i].decode("utf-8").strip("'").split(","), \
                               columns=[i.decode("utf-8")])
            if axis_flag == 1:
                row_wise_df  = pd.concat([row_wise_df,df1])
                #Makes a new column for the Row Key
                row_wise_df[':row_key'] = key.decode("utf-8")
                axis_flag = 0
            else:
                row_wise_df = pd.concat([row_wise_df,df1],axis=1)
        col_list = list(row_wise_df.columns.values)
        row_wise_df.columns = [i.split(':', 1)[1] for i in col_list]


        df_for_each_site = pd.concat([df_for_each_site,row_wise_df])
    df_for_all_site = pd.concat([df_for_all_site ,df_for_each_site])
    #below line commented because, now we don't need to write in csv, 
    #other wise just uncomment following line
    #write_csv_df(df_for_all_site)
    return df_for_all_site

def cradle_get_with_rowkey(table_name,sys_name):
    #This function is almost same as crad_get function with only difference that 
    #this function returns row_key for each data points. 
    """
    This function is almost same as crad_get function with only difference 
    that this function returns row_key column for each data points. In summary 
    this funxtion returns a dataframe for a requested alphanumeric and the 
    dataframe also has a column called row_key in addition to other columns.

    Args:
        arg1 (string): Name of the table to retrieve from. 
        arg2 (string): Name of the alphanumeric (which is prefix of 
                                                 the rowkey). 
    Returns:
        res (dataframe): retirved dataset.
    Examples:
        cradle_get_with_rowkey('ecradle','alphanumeric')
    
    """
    rows = data_download(table_name,sys_name)
    df_for_all_site = pd.DataFrame()
    df_for_each_site = pd.DataFrame()
    final_dict = {}
    for key,data in iter(sorted(rows.items())):
        row_wise_df = pd.DataFrame()
        axis_flag = 1
        for i in data.keys():
            if re.match('ftir',i.decode("utf-8")) or \
                   re.match('spc',i.decode("utf-8")) or \
                   re.match('jpg',i.decode("utf-8")) or \
                   re.match('jpeg',i.decode("utf-8")) or \
                   re.match('png',i.decode("utf-8")) or \
                   re.match('Rda',i.decode("utf-8")) or \
                   re.match('bmp',i.decode("utf-8")) or \
                   re.match('pdf',i.decode("utf-8")) or \
                   re.match('RData',i.decode("utf-8")) or \
                   re.match('RDS',i.decode("utf-8")) or \
                   re.match('srw',i.decode("utf-8")) or \
                   re.match('image',i.decode("utf-8")):
                continue
            df1 = pd.DataFrame(data[i].decode("utf-8").strip("'").split(","), \
                               columns=[i.decode("utf-8")])
            if axis_flag == 1:
                row_wise_df  = pd.concat([row_wise_df,df1], sort=True)
                #Makes a new column for the Row Key
                row_wise_df[':row_key'] = key.decode("utf-8")
                axis_flag = 0
            else:
                row_wise_df = pd.concat([row_wise_df,df1],axis=1, sort=True)
        col_list = list(row_wise_df.columns.values)
        
        #Since we added a new column, 
        #the numbers must be adjusted to prevent an off by one error.
        row_wise_df.columns = [i.split(':', 0)[0] for i in col_list] 
        df_for_each_site = pd.concat([df_for_each_site,row_wise_df], sort=True)
    df_for_all_site = pd.concat([df_for_all_site ,df_for_each_site], sort=True)
    #below line commented , we don't need to write in csv, for pycradletools
    #other wise just uncomment following line
    #write_csv_df(df_for_all_site)
    return df_for_all_site

