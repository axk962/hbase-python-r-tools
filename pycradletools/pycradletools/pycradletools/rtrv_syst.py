import sys
sys.path.append('/home/rxf131/pythonpackages/lib/python3.7.0/site-packages')
import happybase
import glob
import pandas as pd
#import pdb

def crad_fltr(*args):
    """This function takes multiple aggument and renders the 
       dataframe based on the filter on a HBase table.

    Args:
        arg1 (string): Table Name
        arg2 (string): Family Name
        arg3 (string): Column Name
        arg4 (string): Column Value
        arg5 (string): Family Name
        arg6 (string): Column Name
        arg7 (string): Column Value
        arg8 repeats arguments 5,6 and 7

    Returns:
        res (pandas dataframe): output of the filter applied on HBase table 
    Examples:
        pct.crad_fltr('meta','pp','dtyp','pp','pp','styp','ss2')
    """

    #pdb.set_trace()
    table_name = args[0]
    arg_len = len(args)

    connection = happybase.Connection('IP-ADDRESS',port=9090)##hbase connection ip
    table = connection.table(str.encode(table_name))#connection to desired table

    if ((arg_len-1)%3 != 0 and (arg_len-1) >= 3):
        print("Please enter the correct number of argument")
    i = 1
    while (i < arg_len):
        fam_name = args[i]
        col_name = args[i+1]
        col_val = args[i+2]
        if (i == 1):
            #Making query for HBase based on input 
            #parameters. true,false parameters hard coded in the query are for 
            #column filter and version.
            #Making a query string
            qstr = """SingleColumnValueFilter('{0}','{1}',=,'regexstring:{2}',true,false)""".format(fam_name,col_name,col_val)
        else:
            qstr = qstr + """ AND SingleColumnValueFilter('{0}','{1}',=,'regexstring:{2}',true,false)""".format(fam_name,col_name,col_val)
        i=i+3

    #Make an array of column family name
    #Such that filter function returns value only from one column family
    col_fam = fam_name
    
    dictreturn={}
    #for key, data in table.scan(filter=qstr, columns=col_fam, batch_size=1):
    for key, data in table.scan(filter=qstr):
        dictreturn[key] = data
    
    try:
        connection.close()
    except Exception as e:
        print (e)
        print ("\nUnable to close Hbase connection \n")
    
    df_for_all_site = pd.DataFrame()
    df_for_each_site = pd.DataFrame()
    
    for key,data in iter(sorted(dictreturn.items())):
        row_wise_df = pd.DataFrame()
        axis_flag = 1
        for i in data.keys():
            df1 = pd.DataFrame(data[i].decode("utf-8").strip("'").split(","), \
                               columns=[i.decode("utf-8")]) 
            if axis_flag == 1:
                row_wise_df  = pd.concat([row_wise_df,df1], sort=True)
                #Makes a new column for the Row Key
                row_wise_df[':s_name'] = key.decode("utf-8")
                axis_flag = 0
            else:
                row_wise_df = pd.concat([row_wise_df,df1],axis=1, sort=True)
        col_list = list(row_wise_df.columns.values)
        row_wise_df.columns = [i.split(':', 1)[1] for i in col_list]
        df_for_each_site = pd.concat([df_for_each_site,row_wise_df], sort=True)
    df_for_all_site = pd.concat([df_for_all_site ,df_for_each_site], sort=True)
    df_for_all_site.index = range(len(df_for_all_site))
    return df_for_all_site


