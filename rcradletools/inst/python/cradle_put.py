import sys

def cradle_put(table_name,family_name,file_name, batch=True):
    import sys
    sys.path.append('/home/rxf131/pythonpackages/lib/python2.7.13/site-packages')
    import happybase
    import glob
    import pandas as pd
    import pdb
    import shutil
    import time
    import csv
    import os

    #pdb.set_trace()
    connection = happybase.Connection('IP-ADDRESS',port=9090)##hbase connection ip
    table = connection.table(table_name)

    if batch == True:
        #filesPath='/dataframeForPythonToUploadToHBaseFromR.csv'
        filesPath = file_name
        #hd = os.environ['HOME']
        #filesPath = hd + filesPath
        filesList = glob.glob(filesPath)
        #print(filesList)
        for file_name in filesList:
            #print(file_name)
            with open(file_name, mode='r') as f:
                columns = f.readline().replace("\n","").split("~")
                #reader = csv.DictReader
                for r in f:
                    x1=r.replace("\n","").split("~")
                    for i in range(1,len(x1)):
                        col_name = family_name+ ":" + columns[i]
                        col_val = x1[i]
                        row_key = x1[0]
                        with table.batch(batch_size=1) as b:
                            b.put(row_key,{col_name:col_val})
            time.sleep(1)

if __name__ == '__main__':
    cradle_put(sys.argv[1],sys.argv[2],sys.argv[3])
