import sys
sys.path.append('/home/rxf131/pythonpackages/lib/python3.7.0/site-packages')
import happybase
import glob
import pandas as pd
#import pdb
import shutil
import time
import csv
    
def cradle_put(table_name,family_name,df_name=None,batch=False):
    """This function takes in three parameters at a time.
       The purpose of the function is to ingest data from either dataframe
       or a csv file into HBase table.
        
    Args:
        arg1 (string): Name of the table to ingest data. 
        arg2 (string): Family name in the table. 
        arg3 (string): Name of the pandas dataframe to ingest.
        arg4 (string): batch, True/False, If the dataframe is specified
                       in argument 3, then batch must be False, otherwise
                       if dataframe is not specified then batch must be True 
                       and the files should be located at following path
                       /mnt/projects/CSE_MSE_RXF131/data_for_HBase_ingestion/.
                       The files which get ingested into database moves to path
                       /mnt/projects/CSE_MSE_RXF131/data_for_HBase_ingestion/data_ingested_to_Hbase
        Note: Argument 3 and 4 are mutually exclusive.
    Returns:
        Nothing, Ingest data into HBase table 
    Examples:
        I.  cradle_put('table_name','col_fam', df_name=dataframe)
        II. cradle_put('table_name','col_fam', batch=True)
    """

    #pdb.set_trace()
    connection = happybase.Connection('IP-ADDRESS',port=9090)
    table = connection.table(str.encode(table_name))
    
    if batch == True:
        #if batch is True, it looks for files at following path.
        #Path where this function looks for csv files to be ingested
        #into HBase. The csv files should be in the proper format as described
        #in SDLE google sites page.
        filesPath='/mnt/projects/CSE_MSE_RXF131/data_for_HBase_ingestion/*.csv'
        filesList = glob.glob(filesPath)
        for file_name in filesList:
    
            print(file_name)
            with open(file_name, mode='r') as f:
                columns = f.readline().replace("\n","").split("~")
                print(columns)
                #reader = csv.DictReader 
                for r in f:
                    x1=r.replace("\n","").split("~")
                    for i in range(1,len(x1)):
                        #colon is added to the colname because
                        #column name is fam_name:col_name
                        col_name = family_name +':'+ columns[i]
                        col_val = x1[i]
                        row_key = x1[0]
                        with table.batch(batch_size=1) as b:
                            #put command to ingest into HBase
                            b.put(row_key,{col_name:col_val})
            destDir = \
                '/mnt/projects/CSE_MSE_RXF131/data_for_HBase_ingestion'\
                +'/data_ingested_to_Hbase/'\
                +file_name.split("/")[-1]
            #sleep time to clear thrift server que     
            time.sleep(2)
            #moves data to destination dir. from where it 
            #can be moved to HDFS for backup
            shutil.move(file_name,destDir)
    else:
        #else condition is to ingest dataframe directly into HBase
        df = df_name
        nrow,ncol = df.shape
        with table.batch(batch_size = 1) as b:
            for r in range(0,nrow):
                row_key = df.iloc[r][0]
                for c in range(1,ncol):
                    col_name = family_name +':'+ df.columns.values[c]
                    col_val = repr(df.iloc[r][c])
                    #put command to ingest into HBase
                    b.put(row_key,{col_name:col_val})
