import sys
sys.path.append('/home/rxf131/pythonpackages/lib/python3.7.0/site-packages')
import glob
import pandas as pd
#import pdb
from datetime import datetime

from .rtrv_syst import crad_fltr
from .crad_get_rowkey_range import crad_get_rowkey_range 
from .helper_crad_weather import sgis_lat_lon, add_months

def crad_weather(latitude, longitude, start_date, end_date):
    """
        This function takes four aggument and renders the 
        dataframe only if the weather data is in the weather table.
        If the weather weather data is not available, use 'cradlesgis'
        a R package to download weather data.

    Args:
        arg1 (float): latitude, rounded to two decimals
        arg2 (float): longitude, rounded to two decimals
        arg3 (string): Start date of the weather data 'YYYY-MM-DD'
        arg4 (string): End date of the weather data 'YYYY-MM-DD'

    Returns:
        res (pandas dataframe) 
    Examples:
        pct.crad_weather('lat','lon','sd','ed')
    """
    
    TABLE_NAME = 'meta'
    FAMILY_NAME = 'sgis'
    COL_LAT = 'glat'
    COL_LONG = 'glon'
     
    #pdb.set_trace()
    latitude = float(latitude)
    longitude = float(longitude)
    #Get lat. long. pair rounded upto two places ending with even number
    list_lat_lon = sgis_lat_lon(latitude, longitude)
    #get the row key for a solargis data for the lat. long. pair
    weather_meta = crad_fltr(TABLE_NAME, FAMILY_NAME, COL_LAT , list_lat_lon[0], FAMILY_NAME, COL_LONG, list_lat_lon[1])
    if weather_meta.empty:
        print("""Weather Data Does Not exsist for given parameters,
               download using cradlesgis, an R package""")
        return None 
    if datetime.strptime(start_date,'%Y-%m-%d') < datetime.strptime(weather_meta.iloc[0,]['gstr'],'%Y-%m-%d') or \
        datetime.strptime(end_date,'%Y-%m-%d') > datetime.strptime(weather_meta.iloc[0,]['gend'],'%Y-%m-%d'):
        print("""Only partial data is available, 
               download full data from cradlesgis, an R package""")
        return None
    datetimeobject = datetime.strptime(start_date,'%Y-%m-%d')
    start_date = datetimeobject.strftime('%Y%m')
    datetimeobject = datetime.strptime(end_date,'%Y-%m-%d')
    #Adding a month, because hbase crad_get_range function 
    #does not include a last month in retrival.This is due
    # to scan function in hbase. 
    datetimeobject = add_months(datetimeobject)
    end_date = datetimeobject.strftime('%Y%m')
    weather_data = crad_get_range('weather',weather_meta.iloc[0]['s_name'],start_date,end_date)
    return weather_data

