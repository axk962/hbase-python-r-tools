import sys
sys.path.append('/home/rxf131/pythonpackages/lib/python3.7.0/site-packages')
import happybase
import os
from os.path import basename
import glob

def cradle_put_bin(table_name, family_name, col_name, dir_path = None, input_files = None, rowkey = None):
    """
       The purpose of the function is to ingest data from either a directory
       or from list of files into HBase table.
       The filetypes handeled by this function are 
       ('*.jpg', '*.jpeg', '*.png', '*.spc','*.bmp', '*.Rda', '*pdf', '*.RData','*RDS','*srw').
       To add new filetype, edit this function to add new filetypes.
        
    Args:
        arg1 (string): Name of the table to ingest data into. 
        arg2 (string): Family name in the table. 
        arg3 (string/list): Column names of in which the files are ingested 
        arg4 (string): Path of the directory where binary files are located 
        arg5 (list): Filename with a full path 
        arg6 (list): If given then it will be assigned rowkey, 
                       otherwise filename will be rowkey 
    Returns:
        Nothing, Ingest data into HBase table 
    Examples:
        I.  cradle_put_bin('table_name','col_fam', 'col_name', dir_path = 'home/axk962/temp/')
        II.  cradle_put_bin('table_name','col_fam', list_of_col_name, input_files = list_of_input_files,rowkey = list_of_rowkey)
    """
    #import pdb
    #pdb.set_trace()
    #check either one of the parameter is assigned value
    if dir_path is None and input_files is None:
        print('Both dir_path and input_files parameter cannot be None')
        return 1
    elif dir_path is not None and input_files is not None:
        print('Either dir_path or input_files parameter can be passed a value')
        return 1

    #input parameter lists len validation
    if isinstance(input_files, list):
        if isinstance(col_name,list):
            if len(input_files) != len(col_name):
                print('length of the list of input_files should be equal to len of col_name.')
                return 1
        if isinstance(rowkey, list):
            if len(input_files) != len(rowkey):
                print('length of the list of input_files should be equal to len of rowkey.')
                return 1
    connection = happybase.Connection('IP-ADDRESS',port=9090)
    table = connection.table(str.encode(table_name))
    #expected binary data type
    #types = ('*.jpg', '*.jpeg', '*.png', '*.spc','*.bmp', '*.Rda', '*pdf', '*.RData','*RDS','*srw')
    files = []
    #Check input files
    if dir_path is not None:
        #check for file type in directory
        types = ('*.jpg', '*.jpeg', '*.png', '*.spc','*.bmp', '*.Rda', '*pdf', '*.RData','*RDS','*srw')
        for extension in types:
            files.extend(glob.glob(os.path.join(dir_path, extension)))

    elif isinstance(input_files, list):
        files.extend(input_files)
        types = ['dummy']
    elif isinstance(input_files, str):
        files.append(input_files)
        types = ['dummy']
    else:
        print('input_files parameter should be list or str')
        return 1

        if dir_path is not None:
            files.extend(glob.glob(os.path.join(dir_path, extension)))

    for i in range(len(files)):
        #for file_name in files:
        file_name = files[i]
        name, tag = file_name.split(".")
        name = basename(name)
        #if row_key given then re-assign
        if isinstance(rowkey, list):
            name = rowkey[i]
        elif isinstance(rowkey, str):
            name = rowkey
            
        if isinstance(col_name,list):
            col_qual = col_name[i]
        else:
            col_qual = col_name

        #if name not in ingested_files:
        with open(file_name, 'rb') as file:
            content = file.read()
            file.close()
            #Appends the file type extension into the column name and 
            #puts data into HBase table.
            #table.put(name, {b'' + family_name + ':'+tag+'_'+col_qual: content})
            table.put(name, {family_name + ':'+tag+'_'+col_qual: content})
            #ingested_files.append(name)
    sys.stdout.write("ingestion to HBase completed, closing connection... \n")
    connection.close()


#cradle_put_bin(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
