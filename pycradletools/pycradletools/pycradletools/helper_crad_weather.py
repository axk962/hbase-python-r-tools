import sys
sys.path.append('/home/rxf131/pythonpackages/lib/python3.7.0/site-packages')
from rpy2.robjects.packages import importr
import rpy2.robjects as ro

def sgis_lat_lon(lat, lon):
    """
    This is a R helper function and is executed using rpy2
    As of today, this script is not able to run on spyder 
    because of QT4 clash with R.

    This R helper function is for rounding the latitude and 
    the longitude upto two decimal places and ending with 
    even digit.

    This function is to ensure that the rounding of latitude 
    and longitude is same for both R and Python packages. Using the 
    same commands as in cradlesgis package for downloading 
    SolarGIS data, we ensure same rounding numbers for digits
    both in R and python.
    Authors: Ahmad Maroof Karimi
    Created: 06-03-2018
    Modified: 06-03-2018

    Args:
        lat (float): Latitude of the place for sgis data
        lon (float): Longitude of the place for sgis data

    Returns:
        List (str): rounded values of latitude and longitude
        
    """
    ro.r('''
        
        round_lat_lon <- function(lat,lon, verbose=FALSE){
            if (verbose){
                cat("Called an R function round_lat_lon() . \n")
            }
            lat <- round(lat/0.02) * 0.02
            lon <- round(lon/0.02) * 0.02
            #Convert numeric lat/lon to strings, ensure trailing zeroes (e.g. -131.00)
            lat_str <- format(lat, nsmall = 2)
            lon_str <- format(lon, nsmall = 2)
            c(lat_str, lon_str)
        }
    ''')

    r_lat_lon = ro.globalenv['round_lat_lon']
    list_loc = r_lat_lon(lat, lon)
    return list_loc

def add_months(date_time, number_of_months=1):
    from datetime import datetime
    temp_month = date_time.month + number_of_months
    temp_year = date_time.year
    temp_day = date_time.day
    str_date_time = str(temp_year) + '-' + str(temp_month) + '-' + str(temp_day)
    return datetime.strptime(str_date_time,"%Y-%m-%d")
     
