import sys
sys.path.append('/home/rxf131/pythonpackages/lib/python2.7.13/site-packages')
import happybase
import glob
import pandas as pd
import pdb

def write_csv_df(df,rand_file_number):
    #filename = "python_to_r.csv"
    filename = "~/"+rand_file_number+"metadata_logc_cmpr_python_to_r_from_cradletools_package.csv"
    is_file_exist = glob.glob(filename)
    if not is_file_exist:
        df.to_csv(filename,sep='~',index=False)
    else:
        df.to_csv(filename,sep='~',index=False)


def rtrv_syst(args):
    #pdb.set_trace()
    rand_file_number = args[0]
    table_name = args[1]
    arg_len = len(args) - 1

    connection = happybase.Connection('IP-ADDRESS',port=9090)##hbase connection ip
    table = connection.table(table_name)#connection to desired table

    if ((arg_len)%5 != 0 and (arg_len-1) >= 5):
        print("Please enter the correct number of argument")
        return
    #i starts from 2 because
    #args[0] rand_file_number
    #args[1] table_name
    i = 2
    while (i < arg_len):
        if (i == 2):
            fam_name = args[i]
            col_name = args[i+1]
            cmpr = args[i+2]
            col_val = args[i+3]
            #Making a query string
            qstr = """SingleColumnValueFilter('{0}','{1}',{2},'binary:{3}',true,false)""".format(fam_name,col_name,cmpr,col_val)
            #for prefix search
            #qstr = """SingleColumnValueFilter('{0}','{1}',{2},'binaryprefix:{3}')""".format(fam_name,col_name,cmpr,col_val)
            i=i+4
        else:
            logc = args[i]
            fam_name = args[i+1]
            col_name = args[i+2]
            cmpr = args[i+3]
            col_val = args[i+4]
            qstr = qstr + """ {0} SingleColumnValueFilter('{1}','{2}',{3},'binary:{4}',true,false)""".format(logc,fam_name,col_name,cmpr,col_val)
            i=i+5

    #Make an array of column family name
    #Such that filter function returns value only from one column family
    col_fam = [b''+ fam_name]

    dictreturn={}
    for key, data in table.scan(filter=qstr,columns=col_fam, batch_size=1):
        dictreturn[key] = data

    try:
        connection.close()
    except Exception as e:
        print e
        print "\nUnable to close Hbase connection \n"

    df_for_all_site = pd.DataFrame()
    df_for_each_site = pd.DataFrame()

    for key,data in iter(sorted(dictreturn.iteritems())):
        row_wise_df = pd.DataFrame()
        axis_flag = 1
        for i in data.keys():
            df1 = pd.DataFrame(data[i].strip("'").split(","), columns=[i])
            if axis_flag == 1:
                row_wise_df  = pd.concat([row_wise_df,df1])
                axis_flag = 0
            else:
                row_wise_df = pd.concat([row_wise_df,df1],axis=1)
        col_list = list(row_wise_df.columns.values)
        row_wise_df.columns = [i.split(':', 1)[1] for i in col_list]
        row_wise_df['s_name'] = key
        df_for_each_site = pd.concat([df_for_each_site,row_wise_df])
    df_for_all_site = pd.concat([df_for_all_site ,df_for_each_site])
    df_for_all_site.index = range(len(df_for_all_site))
    write_csv_df(df_for_all_site,rand_file_number)

if __name__ == '__main__':
    #pdb.set_trace()
    rtrv_syst(sys.argv[1:])

