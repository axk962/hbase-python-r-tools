import sys
sys.path.append('/home/rxf131/pythonpackages/lib/python3.7.0/site-packages')
import happybase
import glob
import pandas as pd
#import pdb

def write_csv_df(df,sys_name):
    #filename = "python_to_r.csv"
    filename = "~/"+sys_name+"python_to_r_from_cradletools_package.csv"
    is_file_exist = glob.glob(filename)
    if not is_file_exist:
        df.to_csv(filename,sep='~',index=False)
    else:
        df.to_csv(filename,sep='~',index=False)
    #Following codes are comented, if we don't want to overwrite
    #    overwrite = raw_input ("Warning: "+ filename + " already  exists, would you want to overwrite (y/n) \n")
    #    if overwrite == 'y':
    #        df.to_csv(filename,sep=',',index=False)
    #    elif overwrite == "n":
    #        new_filename = raw_input("Type new filename \n")
    #        df.to_csv(new_filename,sep=',',index=False)
    #    else:
    #        print("Not a valid input, no file saved!\n")


def data_download(table_name,roww_strt,roww_endd):
    #pdb.set_trace()
    connection = happybase.Connection('IP-ADDRESS',port=9090)##hbase connection ip
    table = connection.table(str.encode(table_name))#connection to desired table
    l=[] #introducing empty list
    p=[]
    dictreturn={}
    # getting data by table.rows loop
    for key,data in table.scan(row_start = roww_strt,row_stop = roww_endd, batch_size = 10):
        dictreturn[key] = data
    from collections import defaultdict
    try:
        connection.close()
    except Exception as e:
        print (e)
        print ("\nUnable to close Hbase connection \n")
    return dictreturn


def crad_get_rowkey_range(table_name,syst_name,strt_date,endd_date):
    #pdb.set_trace()
    roww_strt = syst_name+"-"+strt_date
    roww_endd = syst_name+"-"+endd_date
 
    rows = data_download(table_name,roww_strt,roww_endd)
    df_for_all_site = pd.DataFrame()
    df_for_each_site = pd.DataFrame()
    final_dict = {}
    for key,data in iter(sorted(rows.items())):
        row_wise_df = pd.DataFrame()
        axis_flag = 1
        for i in data.keys():
            df1 = pd.DataFrame(data[i].decode("utf-8").strip("'").split(","), \
                               columns=[i.decode("utf-8")])
            if axis_flag == 1:
                row_wise_df  = pd.concat([row_wise_df,df1], sort=True)
                #Makes a new column for the Row Key
                row_wise_df[':s_name'] = key.decode("utf-8")
                axis_flag = 0
            else:
                row_wise_df = pd.concat([row_wise_df,df1], axis=1, sort=True)
        col_list = list(row_wise_df.columns.values)
        row_wise_df.columns = [i.split(':', 1)[1] for i in col_list]


        df_for_each_site = pd.concat([df_for_each_site,row_wise_df], sort=True)
    df_for_all_site = pd.concat([df_for_all_site ,df_for_each_site], sort=True)
    #below line commented because, now we don't need to write in csv, other wise just uncomment following line
    #write_csv_df(df_for_all_site,syst_name)
    return df_for_all_site

