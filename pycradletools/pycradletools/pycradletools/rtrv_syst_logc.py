import sys
sys.path.append('/home/rxf131/pythonpackages/lib/python3.7.0/site-packages')
import happybase
import glob
import pandas as pd
#import pdb

def crad_fltr_logc(*args):
    """This function takes multiple aggument and renders the 
       dataframe based on the filter on a HBase table.

    Args:
        arg1 (string): Table Name
        arg2 (string): Family Name
        arg3 (string): Column Name
        arg4 (string): Comparision Operator 
        arg5 (string): Column Value
        arg6 (string): Logical Operator
        arg7 (string): Family Name
        arg8 (string): Column Name
        arg9 (string): Comparision Operator
        arg10 (string): Column Value
        arg11 repeats arguments 6 to 10
    
    Returns:
        res (pandas dataframe): output of the filter applied on HBase table

    Examples:
        crad_fltr_logc('testcra','cra1','brnd','=','EVERGREEN','AND','cra1','sage','>','6')
    """

    #pdb.set_trace()
    table_name = args[0]
    arg_len = len(args)

    connection = happybase.Connection('IP-ADDRESS',port=9090)##hbase connection ip
    table = connection.table(str.encode(table_name))#connection to desired table

    if ((arg_len)%5 != 0 and (arg_len-1) >= 5):
        print("Please enter the correct number of argument")
        return
    i = 1
    while (i < arg_len):
        if (i == 1):
            fam_name = args[i]
            col_name = args[i+1]
            cmpr = args[i+2]
            col_val = args[i+3]
            #Making a query to filter data based on the input parameters
            #true,false in the query string are for column and version in HBase. 
            qstr = """SingleColumnValueFilter('{0}','{1}',{2},'binary:{3}',\
                      true,false)""".format(fam_name,col_name,cmpr,col_val)
            #for prefix search
            #qstr = """SingleColumnValueFilter('{0}','{1}',{2},\
            #     'binaryprefix:{3}')""".format(fam_name,col_name,cmpr,col_val)
            i=i+4
        else:
            logc = args[i]
            fam_name = args[i+1]
            col_name = args[i+2]
            cmpr = args[i+3]
            col_val = args[i+4]
            #true,false are for column and version, in next line
            qstr = qstr + """ {0} SingleColumnValueFilter('{1}','{2}',{3},\
                              'binary:{4}',true,false)""".\
                              format(logc,fam_name,col_name,cmpr,col_val)
            i=i+5


    #Make an array of column family name
    #Such that filter function returns value only from one column family
    col_fam = fam_name

    dictreturn={}
    #filter on the HBase table and return the output in the dictionary
    for key, data in table.scan(filter=qstr, batch_size=1):
        dictreturn[key] = data

    try:
        connection.close()
    except Exception as e:
        print (e)
        print ("\nUnable to close Hbase connection \n")

    df_for_all_site = pd.DataFrame()
    df_for_each_site = pd.DataFrame()

    for key,data in iter(sorted(dictreturn.items())):
        row_wise_df = pd.DataFrame()
        axis_flag = 1
        for i in data.keys():
            df1 = pd.DataFrame(data[i].decode("utf-8").strip("'").split(","), \
                               columns=[i.decode("utf-8")])
            if axis_flag == 1:
                row_wise_df  = pd.concat([row_wise_df,df1], sort=True)
                #Makes a new column for the Row Key
                row_wise_df[':s_name'] = key.decode("utf-8")
                axis_flag = 0
            else:
                row_wise_df = pd.concat([row_wise_df,df1],axis=1, sort=True)
        col_list = list(row_wise_df.columns.values)
        row_wise_df.columns = [i.split(':', 1)[1] for i in col_list]

        df_for_each_site = pd.concat([df_for_each_site,row_wise_df], sort=True)
    df_for_all_site = pd.concat([df_for_all_site ,df_for_each_site], sort=True)
    df_for_all_site.index = range(len(df_for_all_site))
    return df_for_all_site

