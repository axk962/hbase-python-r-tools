#' Simulated time series power_data for test.
#'
#' The simulated time series power data is provided by SDLE for MLEET (Module Level Exposure and Evaluation Test) project funded by DOE (Department of Energy).
#'
#'
#' @docType data
#' @usage data(power_data)
#' @name power_data
#'
#' @format A data frame with 6 rows and 6 variables:
#' \describe{
#'   \item{row_key}{unique identifier for a power plant with suffix as YYYYMM}
#'   \item{iacp}{AC current}
#'   \item{modt}{Module temperature}
#'   \item{temp}{Ambient temperature}
#'   \item{tmst}{Timestamp}
#' }
#'
#' @source Solar Durability and Lifetime Extension (SDLE) Research Center, Case Western
#' Reserve University
"power_data"
