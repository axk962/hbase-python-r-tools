---
title: "Cradle Tools"
author: "Ahmad Maroof Karimi"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Cradle Tools}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

Cradletools package is collection of set of functions which provide an user friendly interface to HBase database. The two main functions of rcradletools can be classified into data query and data register (write).



## Description

Cradle tools package contain six major functions, they are:

  * crad_get - Retrieves data from the HBase tables based on the row key. The returned object is a dataframe having columns as the variables and rows have values for those variable. crad_get function has two input parameters: 1) table name, name of the table from which data is to be retrieve. 2) row_key prefix, a character string for rows from the table. The character string can be alphanumeric, sample numbers etc.
```{r, message=FALSE,eval=FALSE}
  crad_get('table  name', 'row key prefix')
  df <- crad_get('bldg','as8ni6k')
```
  
  * crad_put - Ingest dataframe to the HBase table. The first column of the dataframe must be rowkey in HBase table, and other columns of the dataframe are converted to columns in HBase. This function has three input parameters:1) Table name, 2) Column family and 3) Dataframe to be ingested in HBase. If the rowkeys already exsist, it will create new columns for exsisting rowkeys. If the columns also exsist in the table then data in that column is overwritten as a new version.
```{r, message=FALSE,eval=FALSE}
  crad_put('ecradle','column family',dataframe)
  crad_put('ecradle','pp',dataframe)
```
  * crad_get_bin - This function is used to query data from HBase which are saved in a binary format instead of text format. The function has four parameters: 1) Table name: Name of the from which data is to be retrieved. 2) Family name: Name of the column family in which data is saved. 3) Row_key: Row key is the unique key for which data is to be retrieved. 4)Column name: Column name is also needed in this function to retrieve data.
```{r, message=FALSE, eval=FALSE}
  df <- crad_get_bin('table name','family name','row key','column name')
  df <- crad_get_bin('testcra','cra1','abc1234','colname1')
```
  * crad_put_bin - This function is used to register data in the binary format. The function has six input parameters: 1) Table name: Name of the table in which data will be ingested. 2) Family name: Family qualifier of the table in which data will be ingested, it is a predefined property of the table. 3) Row_key: Unique row_key for each data point in the table. 4) Column name: Name of the column corresponding to the row_key and family qualifier. 5) r_object: Type of robject such as rmd, Rdata etc. 6) Data type: Type of the data such as png, jpg, pdf etc. 
```{r, message=FALSE,eval=FALSE}
crad_put_bin('table name', 'family name', 'row key', 'column name', 'R object', 'R object type')
  crad_put_bin('testcra','cra1','abc1234','colname1', df, 'RData')
```
  * crad_fltr - The main purpose of the filter function is to retrieve key (alphanumeric, sample number, etc) list based on the filtering conditions. The number of input parameters that can be passed to the function depends on the number of filtering conditions but the order of the input parameters are important. First parameter is always a table name and then we pass parameters in the triples (family name, column name and value in the column to be filtered on) format. For examlple:
```{r, message=FALSE, eval=FALSE}
  crad_fltr('meta','pp','styp','ss1')

  crad_fltr('meta','pp','dtyp','pp','pp','styp','ss1')
```
  * crad_fltr_logc - This is an advanced filter function where logical operators and multiple comprarision operators can be passed as an input.
  The examples of the function are given below.


```{r, message=FALSE, eval=FALSE}

  rcradletools::crad_fltr_logc('table_name', 'family_name', 'column_name', 'comparision_operator',  'value', 'logical_operator', 'family_name', 'column_name', 'comparision_operator', 'value')

  rcradletools::crad_fltr_logc('meta', 'pp', 'brnd', '=', 'XYZ', 'AND', 'cra1', 'sage', '>', '6')
```

## Examples

      1. Ingest data frame into test table
      
      library(rcradletools)
      library(dplyr)
      power_data
      p1 <- power_data %>% group_by(row_key) %>% summarise_all( funs(paste(.,collapse=",")))
      rcradletools::crad_put('test_table','test_fam',p1)
      
      2. Retrieve dataframe from the table
      rcradletools::crad_get('test_table','ab67fk6c')
      
      3. Ingest binary data into table. An example showing jpg image ingestion.
      library(imager)
      pv_cell <- imager::load.image('data/pv_cell.jpg')
      rcradletools::crad_put_bin('test_table','test_fam','pvid','column_name',pv_cell,'jpg')


      4. Retrieve binary data from the table
      pv1 <- rcradletools::crad_get_bin('test_table','test_fam','pvid','column_name')
