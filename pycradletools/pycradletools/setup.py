from setuptools import setup

setup(name='pycradletools',
      version='0.3.0',
      description='Interacts with HBase Database and to render and ingest data',
      url='www.python.org/',
      author='Ahmad Maroof Karimi',
      author_email='axk962@case.edu',
      license='GPL3',
      packages=['pycradletools'],
      package_dir={'pycradletools': './pycradletools'},
      package_data={'pycradletools': ['files/data/*','files/docs/*','README.rst']},
      install_requires=['markdown'],
      include_package_data=True,
      zip_safe=False)
