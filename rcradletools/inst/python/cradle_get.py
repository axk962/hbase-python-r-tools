import sys
sys.path.append('/home/rxf131/pythonpackages/lib/python2.7.13/site-packages')
import crad_get as cg
import crad_result as cr
import pdb
def cradle_get(table_name,rand_file_num,sys_name):
    """This function takes in three parameters.

    Args:
        arg1 (string): Name of the table to retrieve from.
        arg2 (string): It is the random file number such that crad_get
                       fun creates unique file
        arg3 (string): Alphanumeric or list of alphanumeric which is the 
                       rowkey or rowkey prefix you want to fetch.
    Returns:
        res (csv_file): retirved dataset.
    """
    #pdb.set_trace()
    if table_name == 'result':
        #seperate call for result because of family name
        family_name = "txt"
        cr.crad_result(table_name, family_name,\
            sys_name, rand_file_num)
    else:
        cg.crad_get(table_name,rand_file_num,sys_name)


if __name__ == '__main__':
    #pdb.set_trace()
    cradle_get(sys.argv[1],sys.argv[2],sys.argv[3:])

