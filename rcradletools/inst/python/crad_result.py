import sys
sys.path.append('/home/rxf131/pythonpackages/lib/python2.7.13/site-packages')
import happybase
import glob
import pandas as pd
import pdb

def write_csv_df(df,uniq_file_prefix):
    #filename = "python_to_r.csv"
    filename = "~/"+uniq_file_prefix+"python_to_r_package.csv"
    is_file_exist = glob.glob(filename)
    if not is_file_exist:
        df.to_csv(filename,sep='~',index=False)
    else:
        df.to_csv(filename,sep='~',index=False)
    #    overwrite = raw_input ("Warning: "+ filename + " already  exists, would you want to overwrite (y/n) \n")
    #    if overwrite == 'y':
    #        df.to_csv(filename,sep=',',index=False)
    #    elif overwrite == "n":
    #        new_filename = raw_input("Type new filename \n")
    #        df.to_csv(new_filename,sep=',',index=False)
    #    else:
    #        print("Not a valid input, no file saved!\n")


def data_download(table_name,row_keys,fam_name):
    #This function downloads data from HBase and returns it as a dictionary
    #pdb.set_trace()
    #import os
    #addr = os.environ['hadoop_cluster']
    connection = happybase.Connection('IP-ADDRESS',port=9090)##hbase connection ip
    table = connection.table(table_name)#connection to desired table
    l=[] #introducing empty list
    p=[]
    dictreturn={}
    # getting data by table.rows loop
    for row_key in row_keys:
        for key,data in table.scan(row_prefix = row_key,columns=[b'' +fam_name],batch_size = 1):
            dictreturn[key] = data
    from collections import defaultdict
    try:
        connection.close()
    except Exception as e:
        print e
        print "\nUnable to close Hbase connection \n"
    return dictreturn


def crad_result(table_name,fam_name,systems,unique_file_num):
    #This Function fetches data and writes the dataframe as a csv file
    #The data is fetched based on table, sys_name, which is alphanumeric
    #and family_name. Unique_file_num is to create a unique file
    import pdb
    #pdb.set_trace()
    hbase_data = data_download(table_name,systems,fam_name)
    df_for_all_site = pd.DataFrame()
    df_for_each_site = pd.DataFrame()
    final_dict = {}
    for key,data in iter(sorted(hbase_data.iteritems())):
        row_wise_df = pd.DataFrame()
        axis_flag = 1
        for i in data.keys():
            df1 = pd.DataFrame(data[i].strip("'").split(","), columns=[i])
            if axis_flag == 1:
                row_wise_df  = pd.concat([row_wise_df,df1])
                axis_flag = 0
            else:
                row_wise_df = pd.concat([row_wise_df,df1],axis=1)
            row_wise_df[':row_key'] = key #Makes a new column for the Row Key
        col_list = list(row_wise_df.columns.values)

	      #Since we added a new column, the numbers must be adjusted to prevent an off by one error.
        row_wise_df.columns = [i.split(':', 0)[0] for i in col_list]


        df_for_each_site = pd.concat([df_for_each_site,row_wise_df])
    df_for_all_site = pd.concat([df_for_all_site ,df_for_each_site])
    df_for_all_site.columns = [i.split(':', 1)[1] for i in col_list]
    #below line commented because, now we don't need to write in csv, other wise just uncomment following line
    write_csv_df(df_for_all_site,unique_file_num)
    #return df_for_all_site
