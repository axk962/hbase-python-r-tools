import sys
sys.path.append('/home/rxf131/pythonpackages/lib/python3.7.0/site-packages')
import happybase
import os
import pandas as pd
import binascii
import skimage
from skimage import img_as_float
import cv2
#import pdb
import re
from PIL import Image
import io
import numpy as np
#def cradle_get_bin():

def cradle_get_bin(table_name, family_name, r_pre, col_name, f_path=None, save = True):
    """This function takes in three parameters.
       The purpose of the function is to retrieve binary Higher order
       data from HBase table.

    Args:
        arg1 (string): Name of the table.
        arg2 (string): Family name in the table.
        arg3 (string/list): Prefix of rowkeys to retrieve data.
        arg4 (string): Column name
        arg5 (string): Path to save the binary files
        arg6 (Boolean): option to save image or just return it
    Returns:
        None.
    Examples:
        I.  cradle_get_bin('table_name','col_fam', 'row_prefix', 'column_name','/home/abc123/temp_data/')
    """
    #pdb.set_trace()
    connection = happybase.Connection('IP-ADDRESS',port=9090)##hbase connection ip
    table = connection.table(str.encode(table_name))
    data_array = []
    name_array = []
    
    if f_path is None:
        f_path = os.path.expanduser("~/")
    
    #If new data type comes, add to this list
    file_type = ["jpg", "jpeg", "bmp", "png", "spc", "Rda", 'pdf', "RData","RDS", "srw"]

    #get the data from HBase
    #If r_pre is a string, convert to list
    if type(r_pre) is str:
        r_pre = r_pre.split(",")
    for r_p in r_pre:
        #for key, data in table.scan(row_prefix=b'' + r_p):
        for key, data in table.scan(row_prefix = str.encode(r_p)):
            #pattern to check for expected file type
            ptrn_list = []
            for k in data.keys():
                ptrn_list.append(k.decode("utf-8"))
            ptrn = re.compile('|'.join(ptrn_list))
            mtch = False
        
            for extension in file_type:
                tmp_str =  str.encode(family_name + ':'+extension+'_' + col_name)
                if ptrn.match(family_name+':'+extension+'_'+col_name):
                    content = data[tmp_str]
                    file_name = f_path + key.decode("utf-8") +'_'+ col_name+ '.' + extension
                    mtch = True
                if (mtch == True):
                    mtch = False
                    if save:
                        with open(file_name, 'wb') as file:
                            file.write(content)
                            name_array.append(file_name)
                            file.close()
                    else:
                        if extension == 'jpg' or 'jpeg' or 'bmp' or 'png':
                            return np.array(Image.open(io.BytesIO(content)))
                        else:
                            raise Exception('Reading filetypes other then images not supported. Set the save option to True')
                            
    connection.close()

#    for val, name in enumerate(name_array):
#        image = cv2.imread(name_array[val], 0)#as greyscale
#        image = img_as_float(image)
#        image = pd.DataFrame(image)
#        data_array.append(image)
#        os.unlink(name_array[val])
#    return data_array
#if __name__ == '__main__':
#    #import pdb
#    #pdb.set_trace()
#    cradle_get_bin(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5]) #returns an array of fp img dataframes
