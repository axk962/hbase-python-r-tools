import sys
sys.path.append('/home/rxf131/pythonpackages/lib/python3.7.0/site-packages')
import happybase
import glob
import pandas as pd
#import pdb
import time
def fetch_rowkey(table_name, range_start='aaaa', range_end='zzzz'):
    """This function returns all the rowkeys between the range 'aaaa'
       and 'zzzz'. Thus by default it returns all the rowkeys and range
       can be shortened to speed up the query execution on HBase table.

    Args:
        arg1 (string): Table Name
        arg2 (string): start range of row key 
        arg3 (string): end range of row key 

    Returns:
        res (list): list of row keys retirved from HBase table 
    Examples:
        pct.fetch_rowkey('meta')
        pct.fetch_rowkey('meta','aaaa','zzzz')

    """
    #pdb.set_trace()
    connection = happybase.Connection('IP-ADDRESS',port=9090)##hbase connection ip
    table = connection.table(str.encode(table_name))#connection to desired table

    #scanner = table.scan( row_start=b''+range_start, row_stop=b''+range_end,\
    scanner = table.scan( row_start=range_start, row_stop=range_end,\
                          filter="KeyOnlyFilter() AND FirstKeyOnlyFilter()",\
                        )
    rk_list = list()
    for row_key, data in scanner:
        rk_list.append(row_key.decode("utf-8"))
    return rk_list
