"""
The pyradletools package has six functions:
1. cradle_get: To retieve data from HBase and returns as a dataframe
from .rtrv_syst_logc import crad_fltr_logc
from .rtrv_syst import crad_fltr
from .crad_putt_bin import crad_putt_bin
from .crad_get_bin import crad_get_bin
2. cradle_put: To save the dataframe or csv file into HBase.
3. cradle_getbin: To retireve the binary data/files from HBase
4. cradle_putbin: To store binary files in the HBase table.
5. Filters on HBase Table based on family_name, column_name and column value
6. Advanced filter on HBase table based on family_name, column_name, comparision operator, column value and logical operator
7. cradle_get_with_rowkey is similar as cradle_get but it also returns a column with rowkey.
"""
from .cradle_put import cradle_put
from .cradle_get import cradle_get
from .cradle_get_bin import cradle_get_bin
from .cradle_put_bin import cradle_put_bin
from .rtrv_syst_logc import crad_fltr_logc
from .rtrv_syst import crad_fltr
from .crad_get import cradle_get_with_rowkey 
from .crad_weather import crad_weather
from .fetch_rowkey import fetch_rowkey
from .crad_get_rowkey_range import crad_get_rowkey_range
def readme():
  """This function displays the contents of the README.rst file.

  Args:
      NULL (NA): There are no parameters.

  Returns:
    NULL: There are no returns, a print statement is executed.
  """
  import os
  this_dir, this_filename = os.path.split(__file__)
  DATA_PATH = os.path.join(this_dir, "../README.rst")
  with open(DATA_PATH) as f:
      print(f.read())
