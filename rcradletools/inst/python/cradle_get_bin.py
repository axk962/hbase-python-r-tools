import sys
sys.path.append('/home/rxf131/pythonpackages/lib/python2.7.13/site-packages')
import happybase
import os
import pandas as pd
import binascii
import skimage
from skimage import img_as_float
import cv2
import pdb
import re

#def cradle_get_bin():

def cradle_get_bin(table_name, family_name, r_pre,col_name,rand_file_num):
    """This function takes in three parameters.
       The purpose of the function is to retrieve binary Higher order
       data from HBase table.

    Args:
        arg1 (string): Name of the table.
        arg2 (string): Family name in the table.
        arg3 (string): The row_key to retrieve data.
        arg4 (string): The column name.
        arg5 (string): self genrated by R code.
    Returns:
        returns an R object.
    Examples:
        I.  cradle_get_bin('result','bin', 'abc1234', 'column_name')
    """

    connection = happybase.Connection('IP-ADDRESS',port=9090)##hbase connection ip
    table = connection.table(table_name)
    data_array = []
    name_array = []


    tmp_dir = os.path.expanduser("~/")
    file_type = ["jpg", "jpeg", "bmp", "png", "spc", "Rda", 'pdf', "RData","RDS", "srw"]

    #get the data from HBase
    data = table.row(b'' + r_pre)
    #pattern to check for expected file type
    ptrn = re.compile('|'.join(data.keys()))
    mtch = False

    for extension in file_type:
        if ptrn.match(family_name+':'+extension+'_'+col_name):
            content = data[b'' + family_name + ':'+extension+'_' + col_name]
            file_name = tmp_dir + rand_file_num +r_pre +'_'+ col_name+ '.' + extension
            mtch = True

        if (mtch == True):
            with open(file_name, 'w') as file:
                file.write(content)
                name_array.append(file_name)
                file.close()
    connection.close()



#    for val, name in enumerate(name_array):
#        image = cv2.imread(name_array[val], 0)#as greyscale
#        image = img_as_float(image)
#        image = pd.DataFrame(image)
#        data_array.append(image)
#        os.unlink(name_array[val])
#    return data_array
if __name__ == '__main__':
    #import pdb
    #pdb.set_trace()
    cradle_get_bin(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5]) #returns an array of fp img dataframes
